package main

import (
	"flag"
	"log"
	"net/http"
	"os"

	"code.ivysaur.me/contented"
)

func main() {
	cwd, _ := os.Getwd()

	var (
		listenAddr             = flag.String("listen", "127.0.0.1:80", "IP/Port to bind server")
		dataDir                = flag.String("data", cwd, "Directory for stored content")
		dbPath                 = flag.String("db", "contented.db", "Path for metadata database")
		appTitle               = flag.String("title", "contented", "Title used in web interface")
		maxUploadMb            = flag.Int("max", 8, "Maximum size of uploaded files in MiB (set zero for unlimited)")
		maxUploadSpeed         = flag.Int("speed", 0, "Maximum upload speed in bytes/sec (set zero for unlimited)")
		trustXForwardedFor     = flag.Bool("trustXForwardedFor", false, "Trust X-Forwarded-For reverse proxy headers")
		enableHomepage         = flag.Bool("enableHomepage", true, "Enable homepage (disable for embedded use only)")
		enableUpload           = flag.Bool("enableUpload", true, "Enable uploads (disable for read-only mode)")
		diskFilesWorldReadable = flag.Bool("diskFilesWorldReadable", false, "Save files as 0644 instead of 0600")
		maxConcurrentThumbs    = flag.Int("concurrentthumbs", contented.DEFAULT_MAX_CONCURRENT_THUMBS, "Simultaneous thumbnail generation")
		s3Host                 = flag.String("s3hostname", "", "S3 Server hostname")
		s3AccessKey            = flag.String("s3access", "", "S3 Access key")
		s3SecretKey            = flag.String("s3secret", "", "S3 Secret key")
		s3Bucket               = flag.String("s3bucket", "", "S3 Bucket")
		s3Prefix               = flag.String("s3prefix", "", "S3 object prefix")
	)

	flag.Parse()

	opts := contented.ServerOptions{
		DBPath:                 *dbPath,
		BandwidthLimit:         int64(*maxUploadSpeed),
		TrustXForwardedFor:     *trustXForwardedFor,
		EnableHomepage:         *enableHomepage,
		EnableUpload:           *enableUpload,
		DiskFilesWorldReadable: *diskFilesWorldReadable,
		MaxConcurrentThumbs:    *maxConcurrentThumbs,
		ServerPublicProperties: contented.ServerPublicProperties{
			AppTitle:       *appTitle,
			MaxUploadBytes: int64(*maxUploadMb) * 1024 * 1024,
		},
	}

	if len(*s3AccessKey) > 0 {
		opts.StorageType = contented.STORAGE_S3
		opts.DataS3Options.Hostname = *s3Host
		opts.DataS3Options.AccessKey = *s3AccessKey
		opts.DataS3Options.SecretKey = *s3SecretKey
		opts.DataS3Options.Bucket = *s3Bucket
		opts.DataS3Options.Prefix = *s3Prefix

	} else if len(*dataDir) > 0 {
		opts.StorageType = contented.STORAGE_LOCAL
		opts.DataDirectory = *dataDir

	} else {
		log.Println("Please specify either the -data or -s3__ options.")
		os.Exit(1)
	}

	svr, err := contented.NewServer(&opts)
	if err != nil {
		log.Println(err.Error())
		os.Exit(1)
	}

	err = http.ListenAndServe(*listenAddr, svr)
	if err != nil {
		log.Println(err.Error())
		os.Exit(1)
	}
}
