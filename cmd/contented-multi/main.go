package main

import (
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"os"
	"sync"

	"code.ivysaur.me/contented"
)

type ContentedMultiCfg struct {
	Servers []struct {
		ListenAddr string
		Options    contented.ServerOptions
	}
}

func main() {
	configFile := flag.String("config", "contented-multi.cfg", "Path to configuration file")
	flag.Parse()

	fh, err := os.Open(*configFile)
	if err != nil {
		panic(err)
	}

	var cfg ContentedMultiCfg
	err = json.NewDecoder(fh).Decode(&cfg)
	if err != nil {
		panic(err)
	}

	fh.Close()

	wg := sync.WaitGroup{}
	wg.Add(len(cfg.Servers))

	for i, _ := range cfg.Servers {
		go (func(i int) {
			defer wg.Done()

			s, err := contented.NewServer(&cfg.Servers[i].Options)
			if err != nil {
				log.Printf("Failed to create server %d/%d: %s", i+1, len(cfg.Servers), err.Error())
				return
			}

			err = http.ListenAndServe(cfg.Servers[i].ListenAddr, s)
			log.Printf("Server %d/%d shutting down: %s", i+1, len(cfg.Servers), err.Error())

		})(i)
	}

	wg.Wait()
}
