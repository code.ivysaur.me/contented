#
# Makefile for contented
#

VERSION:=1.2.2

SOURCES:=Makefile \
	static \
	cmd $(wildcard cmd/contented/*.go) \
	$(wildcard *.go)

GOFLAGS := -ldflags='-s -w -X code.ivysaur.me/contented.SERVER_HEADER=contented/${VERSION}' -gcflags='-trimpath=$(GOPATH)' -asmflags='-trimpath=$(GOPATH)'

#
# Phony targets
#
	
.PHONY: all dist clean

all: build/linux64/contented build/win32/contented.exe

dist: \
	_dist/contented-$(VERSION)-linux64.tar.gz \
	_dist/contented-$(VERSION)-src.zip

clean:
	if [ -d ./build ] ; then rm -r ./build ; fi
	if [ -f ./contented ] ; then rm ./contented ; fi

#
# Release artefacts
#
	
build/linux64/contented: $(SOURCES) staticResources.go
	mkdir -p build/linux64
	(cd cmd/contented ; \
		CGO_ENABLED=1 GOOS=linux GOARCH=amd64 \
		go build $(GOFLAGS) -o ../../build/linux64/contented \
	)
	
_dist/contented-$(VERSION)-linux64.tar.gz: build/linux64/contented
	mkdir -p _dist
	tar caf _dist/contented-$(VERSION)-linux64.tar.gz -C build/linux64 contented --owner=0 --group=0
