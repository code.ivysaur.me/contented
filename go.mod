module code.ivysaur.me/contented

require (
	code.ivysaur.me/thumbnail v1.0.2
	github.com/mxk/go-flowrate v0.0.0-20140419014527-cca7078d478f
	github.com/speps/go-hashids v1.0.0
	go.etcd.io/bbolt v1.3.7
)

require (
	code.ivysaur.me/imagequant/v2 v2.12.6 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/klauspost/compress v1.16.0 // indirect
	github.com/klauspost/cpuid/v2 v2.2.4 // indirect
	github.com/minio/md5-simd v1.1.2 // indirect
	github.com/minio/minio-go/v7 v7.0.52 // indirect
	github.com/minio/sha256-simd v1.0.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/rs/xid v1.4.0 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/crypto v0.6.0 // indirect
	golang.org/x/image v0.0.0-20200618115811-c13761719519 // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)

go 1.19
