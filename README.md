# contented

[![](doc/image1.thumb.png)](doc/image1.png)

A file / image / paste upload server with a focus on embedding.

You can use contented as a standalone upload server, or you can use the SDK to embed its upload widget into another website.

The name is a pun on "content" and the -d suffix for server daemons.

## Features

- Use local disk or S3-backed storage
- Drag and drop upload
- Multiple files upload
- Pastebin upload
- Custom drawing upload ([via drawingboard.js](https://github.com/Leimi/drawingboard.js))
- Ctrl-V upload
- Galleries and nested galleries
- SDK-oriented design for embedding, including CORS support
- Mobile friendly HTML interface
- Preserves uploaded filename and content-type metadata
- Hash verification (SHA512/256)
- Detect duplicate upload content and reuse storage
- Options to limit the upload filesize, upload bandwidth, and maximum source filesize for thumbnailing
- Short URLs (using [Hashids](http://hashids.org) algorithm)
- Image thumbnailing
- Optional multi-tenant binary (`contented-multi`)

## Usage (Server)

```
Usage of contented:
  -concurrentthumbs int
        Simultaneous thumbnail generation (default 16)
  -data string
        Directory for stored content (default ".")
  -db string
        Path for metadata database (default "contented.db")
  -diskFilesWorldReadable
        Save files as 0644 instead of 0600
  -enableHomepage
        Enable homepage (disable for embedded use only) (default true)
  -enableUpload
        Enable uploads (disable for read-only mode) (default true)
  -listen string
        IP/Port to bind server (default "127.0.0.1:80")
  -max int
        Maximum size of uploaded files in MiB (set zero for unlimited) (default 8)
  -speed int
        Maximum upload speed in bytes/sec (set zero for unlimited)
  -title string
        Title used in web interface (default "contented")
  -trustXForwardedFor
        Trust X-Forwarded-For reverse proxy headers
```

If you are hosting behind a reverse proxy, remember to set its post body size parameter appropriately (e.g. `client_max_body_size` for nginx).

## Usage (HTTP)

The server responds on the following URLs:

URL                  |Method |Description
---------------------|-------|---
`/get/{ID}`          |`GET`  |Download item content
`/p/{ID}`            |`GET`  |Preview item content (HTML)
`/p/{ID}-{ID}-...`   |`GET`  |Preview multiple item content (HTML)
`/info/{ID}`         |`GET`  |Get item content metadata (JSON)
`/thumb/{Type}/{ID}` |`GET`  |Get item thumbnail image (JPEG). "Type" should match `[sbtmlh]`.
`/about`             |`GET`  |Get server metadata (JSON)

## Usage (Embedding for web)

Your webpage should load the SDK from the contented server, then call the `contented.init` function to display the upload widget over the top of an existing DOM element:

```html
<script type="text/javascript" src="SERVER_ADDR/sdk.js"></script>
contented.init("#target");
```

You can optionally supply additional ordered parameters to `contented.init`:

1. A callback, that will be passed an array of file IDs of any uploaded items
2. A callback, that will be called if the SDK widget is closed

## Changelog

2023-05-20: 1.5.1
- Improve support for albums with no images, and for albums with missing interior images

2023-05-19: 1.5.0
- Feature: Support S3-backed storage
- Feature: New `contented-multi` binary to host multiple server configurations from a single process
- Enhancement: Better client-side caching for thumbnails
- Option to cap source filesize for thumbnailing (default 20MiB)

2023-05-17: 1.4.0
- BREAKING: Remove support for some old web browsers (require jQuery 3, ES6 template literals, Promises, Canvas.toBlob)
- Feature: Initial album support with custom titles
- Feature: Support readonly mode
- Enhancement: Use lazy-loading for large image galleries
- Enhancement: Better tab titles on preview pages
- Fix an issue with continuing server-side thumbnailing work even if the http client has gone away
- Fix an issue with not warning on colliding hashIDs
- Internal: Refactor the SDK's initialization phase
- Internal: Update bolt library dependency

2020-07-25: 1.3.1
- Fix an issue with dependencies causing failure to compile in Modules mode

2020-07-25: 1.3.0
- Feature: Option to limit concurrent thumbnail generation
- Enhancement: Set charset=UTF-8 when serving user-submitted text/plain content
- Fix an issue with large memory usage for multipart file uploads

2018-06-09: 1.2.1
- Feature: Add OpenGraph tags on preview pages, for rich metadata in chat applications
- Update thumbnailing library to improve quality
- Use dep for vendoring
- [⬇️ contented-1.2.1-win32.7z](https://git.ivysaur.me/attachments/88dea4f7-e314-4325-a957-096dcf8cdecc) *(1.51 MiB)*
- [⬇️ contented-1.2.1-src.zip](https://git.ivysaur.me/attachments/6fd2b963-3be4-48a6-a5bf-6f273bcaea24) *(1.49 MiB)*
- [⬇️ contented-1.2.1-linux64.tar.gz](https://git.ivysaur.me/attachments/c536f764-0250-4d67-886a-4797946e1124) *(2.21 MiB)*

2017-11-18: 1.2.0
- Feature: Thumbnail support
- Feature: File preview page
- Feature: Album mode (via URL `/p/{file1}-{file2}-...`)
- Feature: New `-diskFilesWorldReadable` option to save files with `0644` mode
- [⬇️ contented-1.2.0-win32.7z](https://git.ivysaur.me/attachments/f3453b62-b2a7-4e77-9b04-44c99dec35ba) *(1.36 MiB)*
- [⬇️ contented-1.2.0-src.zip](https://git.ivysaur.me/attachments/a6c1ecfb-fd6a-44b5-9dc8-aea7c439d1e6) *(178.94 KiB)*
- [⬇️ contented-1.2.0-linux64.tar.gz](https://git.ivysaur.me/attachments/6234754b-af17-4a72-8b66-56a5db21c7c7) *(2.03 MiB)*

2017-10-15: 1.1.0
- Feature: Drawing mode
- Feature: Ctrl+V image upload
- Feature: Option to trust X-Forwarded-For headers when using a reverse proxy
- Feature: Add `getDownloadURL`, `getInfoJSONURL`, `getPreviewURL` SDK methods
- Feature: Option to disable uploading via the homepage
- Feature: Add button to repeat when uploading from homepage
- Enhancement: Automatically load library dependencies
- Enhancement: Display homepage widget using the full screen size
- Include drawingboard.js 0.4.6 (MIT license)
- Fix a cosmetic issue with javascript console output
- Fix a cosmetic issue with error messages if an upload failed
- [⬇️ contented-1.1.0-win32.7z](https://git.ivysaur.me/attachments/bfb0a7fe-bf95-4d0e-933b-8137bc8071a4) *(1.11 MiB)*
- [⬇️ contented-1.1.0-src.zip](https://git.ivysaur.me/attachments/67401341-724f-4ea2-b9c7-44d08ab9d38a) *(142.82 KiB)*
- [⬇️ contented-1.1.0-linux64.tar.gz](https://git.ivysaur.me/attachments/a13752dd-5228-4830-b61d-0f7cc568b2ae) *(1.67 MiB)*

2017-10-08: 1.0.1
- Fix an issue with CORS preflight requests
- Fix an issue with index URLs
- [⬇️ contented-1.0.1-win32.7z](https://git.ivysaur.me/attachments/a873d510-da09-4797-95e9-ffcad690a77b) *(1.10 MiB)*
- [⬇️ contented-1.0.1-src.zip](https://git.ivysaur.me/attachments/43ac17d6-b6f1-4da7-98e9-b8af6fb5551a) *(109.08 KiB)*
- [⬇️ contented-1.0.1-linux64.tar.gz](https://git.ivysaur.me/attachments/34d74bed-db3f-4cef-a76f-266f0b9e6017) *(1.65 MiB)*

2017-10-08: 1.0.0
- Initial public release
- Include jQuery 1.12.4 (MIT license)
- [⬇️ contented-1.0.0-win32.7z](https://git.ivysaur.me/attachments/4ef132cf-dac8-4bcf-9da7-14ca1366e815) *(1.10 MiB)*
- [⬇️ contented-1.0.0-src.zip](https://git.ivysaur.me/attachments/74d77b3f-557b-44bf-9645-7b3b25ab17c1) *(102.45 KiB)*
- [⬇️ contented-1.0.0-linux64.tar.gz](https://git.ivysaur.me/attachments/1c28a913-686b-44cf-b63d-db22968a93b6) *(1.65 MiB)*
