package contented

import (
	"log"
	"net/http"
	"os"
)

func (this *Server) handleView(w http.ResponseWriter, r *http.Request, fileID string) {
	err := this.handleViewInternal(w, r, r.URL.Path[len(downloadUrlPrefix):])
	if err != nil {
		log.Printf("%s View failed: %s\n", this.remoteIP(r), err.Error())
		if os.IsNotExist(err) {
			http.Error(w, "File not found", 404)
		} else {
			http.Error(w, "Couldn't provide content", 500)
		}
	}
}

func (this *Server) handleViewInternal(w http.ResponseWriter, r *http.Request, fileID string) error {

	// Load metadata
	m, err := this.Metadata(fileID)
	if err != nil {
		return err
	}

	// Load file
	f, err := this.ReadFile(r.Context(), m.FileHash)
	if err != nil {
		return err
	}

	defer f.Close()

	// ServeContent only uses the filename to get the mime type, which we can
	// set accurately (including blacklist)

	switch m.MimeType {
	case `text/plain`:
		w.Header().Set(`Content-Type`, `text/plain; charset=UTF-8`)

	case `application/octet-stream`:
		w.Header().Set(`Content-Type`, m.MimeType)
		w.Header().Set(`Content-Disposition`, `attachment; filename="`+m.Filename+`"`)

	default:
		w.Header().Set(`Content-Type`, m.MimeType)
	}

	/*
		if _, ok := f.(io.ReadSeeker); ! ok {
			// Stream directly, no support for bytes/etag
			w.Header().Set(`Content-Length`, fmt.Sprintf("%d", m.FileSize))
			_, err := io.Copy(w, f)
			return err

		}
	*/

	// Allow range requests, if-modified-since, and so on
	http.ServeContent(w, r, "", m.UploadTime, f)
	return nil

}
