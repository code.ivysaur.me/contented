package contented

import (
	"context"
	"encoding/json"
	"fmt"
	"html"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

func (this *Server) handlePreview(ctx context.Context, w http.ResponseWriter, fileIDList string) {

	fileIDs := strings.Split(fileIDList, `-`)

	// Early get metadata for the first listed element
	specialTitle := ""
	if len(fileIDs) == 1 {
		mFirst, err := this.Metadata(fileIDs[0])
		if err != nil { // Same error handling as below -
			if os.IsNotExist(err) {
				http.Error(w, "Not found", 404)
				return
			}

			log.Println(err.Error())
			http.Error(w, "Internal error", 500)
			return
		}

		specialTitle = mFirst.Filename + " (" + fileIDs[0] + ")"
	} else {
		specialTitle = fmt.Sprintf("%d images", len(fileIDs))
	}

	tmpl := `<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
	<head>
		<title>` + html.EscapeString(specialTitle+" | "+this.opts.ServerPublicProperties.AppTitle) + `</title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:title" content="` + html.EscapeString(this.opts.ServerPublicProperties.AppTitle) + `" />
		<meta property="og:site_name" content="` + html.EscapeString(this.opts.ServerPublicProperties.AppTitle) + `" />
		<meta property="og:type" content="website" />
`

	if len(this.opts.ServerPublicProperties.CanonicalBaseURL) > 0 {
		tmpl += `
		<meta property="og:url" content="` + html.EscapeString(this.opts.ServerPublicProperties.CanonicalBaseURL+`p/`+fileIDList) + `" />
		`
	}

	for _, fileID := range fileIDs {
		tmpl += `
		<meta property="og:image" content="` + html.EscapeString(`/thumb/m/`+fileID) + `" />
		<meta property="og:image:type" content="image/jpeg" />
		<meta property="og:image:width" content="300" />
		<meta property="og:image:height" content="300" />
		`
	}

	tmpl += `
		<style type="text/css">
html, body {
	background: #333;
	color: #F0F0F0;
	font-family: sans-serif;
}
.entry {
	display: inline-block;
	margin: 4px;
	border-radius: 4px;
	max-width: 340px;
}
.thumbnail {
	line-height: 0;
	width: 340px;
	height: 340px;
	text-align: center;
	position: relative;
}
.thumbnail-overlay {
	position: absolute;
	bottom: 4px;
	right: 4px;
	padding: 4px 8px;
	line-height: 1.5em;
	
	pointer-events: none;
	
	background: red;
	color: white;
}
.properties {
	background: #000;
	padding: 4px;
	word-break: break-word;
}

		</style>
	</head>
	<body>
		<div class="container">
	`

	for _, fileID := range fileIDs {
		m, err := this.Metadata(fileID)
		if err != nil {
			if os.IsNotExist(err) {

				// If this is just one image out of many, show a 404 box and continue to show the other entries
				// But if this is only a single image requested, abandon the whole pageload

				if len(fileIDs) == 1 {
					http.Error(w, "Not found", 404)
					return
				}

				tmpl += `
					<div class="entry">			
						<div class="thumbnail">
							<img loading="lazy" src="/nothumb_340.png"></a>
						</div>
						<div class="properties">
							Requested ID ` + html.EscapeString(fileID) + ` not found in storage (404)
						</div>
					</div>
				`

				continue
			}

			log.Println(err.Error())
			http.Error(w, "Internal error", 500)
			return
		}

		if m.MimeType == ALBUM_MIMETYPE {
			// Special handling for albums
			f, err := this.ReadFile(ctx, m.FileHash)
			if err != nil {
				log.Printf("Opening file '%s' for preview of album '%s': %s", m.FileHash, fileID, err.Error())
				http.Error(w, "Internal error", 500)
				return
			}

			var childIDs []string
			err = json.NewDecoder(f).Decode(&childIDs)
			f.Close()
			if err != nil {
				log.Printf("Failed to parse album '%s': %s", fileID, err)
				http.Error(w, "Internal error", 500)
				return
			}

			albumThumb := `/nothumb_340.png`
			if len(childIDs) > 0 {
				albumThumb = `/thumb/m/` + childIDs[0]
			}

			tmpl += `
				<div class="entry">			
					<div class="thumbnail">
						<a href="` + html.EscapeString(`/p/`+strings.Join(childIDs, `-`)) + `"><img loading="lazy" src="` + html.EscapeString(albumThumb) + `"></a>
						<div class="thumbnail-overlay">` + fmt.Sprintf("%d", len(childIDs)) + ` image(s)</div>
					</div>
					<div class="properties">
						<b>Name:</b> ` + html.EscapeString(m.Filename) + `<br>
						<b>Hash:</b> <span title="` + html.EscapeString(m.FileHash) + `">hover</span><br>
						<b>File type:</b> Album<br>
						<b>Size:</b> ` + fmt.Sprintf("%d", len(childIDs)) + ` image(s)<br>
						<b>Uploader:</b> ` + html.EscapeString(m.UploadIP) + `<br>
						<b>Uploaded at:</b> ` + html.EscapeString(m.UploadTime.Format(time.RFC3339)) + `<br>
					</div>
				</div>
			`

		} else {
			tmpl += `
				<div class="entry">			
					<div class="thumbnail">
						<a href="` + html.EscapeString(`/get/`+fileID) + `"><img loading="lazy" src="` + html.EscapeString(`/thumb/m/`+fileID) + `"></a>
					</div>
					<div class="properties">
						<b>Name:</b> ` + html.EscapeString(m.Filename) + `<br>
						<b>Hash:</b> <span title="` + html.EscapeString(m.FileHash) + `">hover</span><br>
						<b>File type:</b> ` + html.EscapeString(m.MimeType) + `<br>
						<b>Size:</b> ` + html.EscapeString(fmt.Sprintf("%d", m.FileSize)) + `<br>
						<b>Uploader:</b> ` + html.EscapeString(m.UploadIP) + `<br>
						<b>Uploaded at:</b> ` + html.EscapeString(m.UploadTime.Format(time.RFC3339)) + `<br>
					</div>
				</div>
			`
		}
	}

	if this.opts.EnableHomepage {
		tmpl += `
			<div class="return">
				<button onclick="window.location.href='/'">Again...</button>
			</div>
		`
	}
	tmpl += `
		</div>
	</body>
</html>`

	w.Header().Set(`Content-Type`, `text/html; charset=UTF-8`)
	w.Header().Set(`Content-Length`, fmt.Sprintf("%d", len(tmpl)))
	w.WriteHeader(200)
	w.Write([]byte(tmpl))
}
